<header id="header" class="header-transparent header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 70, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-top-0 appear-animation" data-appear-animation="fadeIn">
        <div class="header-top">
            <div class="container">
                <div class="header-row">
                    <div class="header-column justify-content-start">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <span class="ws-nowrap custom-text-color-grey-1 text-2 pl-0"><i class="icon-envelope icons text-4 top-3 left-1 mr-1"></i> <a href="mailto:codenya.studio@gmail.com" class="text-color-default text-color-hover-primary">codenya.studio@gmail.com</a></span>
                                    </li>
                                    <li class="nav-item d-none d-md-block">
                                        <span class="ws-nowrap custom-text-color-grey-1 text-2"><i class="icon-clock icons text-4 top-3 left-1 mr-1"></i> Mon - Sat 10:00am - 3:00pm / Sunday - CLOSED</span>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="header-column justify-content-end">
                        <div class="header-row">
                            <ul class="header-social-icons social-icons social-icons-clean d-none d-lg-block mr-3">
                                <li class="social-icons-twitter"><a href="https://api.whatsapp.com/send?phone=923335003357" target=" _blank" title="Twitter"><i class="fab fa-whatsapp" style="font-weight: 900!important;"></i></a></li>
                                <li class="social-icons-instagram"><a href="mailto:codenya.studio@gmail.com" target="_blank" title="Gmail"><i class="fas fa-envelope"></i></a></li>
                                <li class="social-icons-facebook"><a href="https://www.facebook.com/CodenyaPK" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                            </ul>
                            <a href="tel:+923335003357" class="btn btn-tertiary font-weight-semibold text-3 px-4 custom-height-1 rounded-0 align-items-center d-none d-md-flex">
                                <i class="icon-phone icons text-4 mr-2"></i> (+92) 333-5003357
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="./index.php">
                                <img alt="Codenya Studio Software And Design Solution" width="180" height="40" src="img/logo.png">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-links order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="nav-link active" href="./index.php">
                                                Home
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle" href="services.php">
                                                Services
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a class="nav-link" href="services.php">Overview</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="android-app-development.php">Android App Development</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="ios-app-development-services.php">iOS App Development</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="mobile-app-design-services.php">Mobile App Design
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="mobile-app-marketing-services.php">Mobile App Marketing
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="web-development-services.php">Web Development</a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="search-engine-optimization-services.php">Search Engine Optimization
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="nav-link" href="cross-platform-mobile-development-services.php">
                                                        Cross Platform Mobile Development
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- <li>
                                            <a class="nav-link" href="demo-seo-2-blog.html">
                                                Blog
                                            </a>
                                        </li> -->
                                        <li>
                                            <a class="nav-link" href="./about-us.php">
                                                About us
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="./contact-us.php">
                                                Contact Us
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                        <!-- <div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                            <div class="header-nav-feature header-nav-features-search d-inline-flex"> <a href="#" class="header-nav-features-toggle" data-focus="headerSearch"><i class="fas fa-search header-nav-top-icon"></i></a>
                                <div class="header-nav-features-dropdown" id="headerTopSearchDropdown">
                                    <form role="search" >
                                        <div class="simple-search input-group"> <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search..."> <span class="input-group-append"> <button class="btn" type="submit"> <i class="fa fa-search header-nav-top-icon"></i> </button> </span> </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>