<section class="section section-height-3 bg-color-secondary position-relative border-0 m-0">
    <div class="container position-relative z-index-1 pt-2 pb-5 mt-3 mb-5">
        <div class="row justify-content-center mb-3">
            <div class="col-md-8 col-lg-6 text-center">
                <div class="overflow-hidden mb-2">
                    <h2 class="font-weight-bold text-color-light text-7 line-height-2 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="250">
                        JOIN OUR LIST
                    </h2>
                </div>
                <div class="overflow-hidden mb-1">
                    <p class="lead custom-text-color-light-1 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">Sign up here to get the latest news, updates and special offers</p>
                </div>
                <div class="overflow-hidden mb-3">
                    <!-- <p class="custom-text-color-light-1 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="550">Best SEO Features & Methodologies. Better SEO than your competitors</p> -->
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <form class="custom-form-style-1 custom-form-simple-validation form-errors-light" action="https://preview.oklerthemes.com/">
                    <div class="form-row mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="700">
                        <div class="form-group col-md-6 pr-md-2">
                            <input type="text" class="form-control" value="" placeholder="Enter Name" required />
                        </div>
                        <div class="form-group col-md-6 pl-md-2">
                            <input type="email" class="form-control" value="" placeholder="Enter E-mail Adress" required />
                        </div>
                    </div>
                    <div class="form-row justify-content-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="850">
                        <div class="form-group col-auto mb-0">
                            <button type="submit" class="btn btn-quaternary btn-rounded font-weight-bold px-5 py-3 text-3">CHECK NOW</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>