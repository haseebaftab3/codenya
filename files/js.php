<!-- Vendor -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="assets/vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="assets/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-seo-2.less"></script>
<script src="assets/vendor/popper/umd/popper.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/common/common.min.js"></script>
<script src="assets/vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="assets/vendor/isotope/jquery.isotope.min.js"></script>
<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="assets/vendor/vide/jquery.vide.min.js"></script>
<script src="assets/vendor/vivus/vivus.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="assets/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



<!-- Demo -->
<script src="assets/js/demos/demo-seo-2.js"></script>


<!-- Theme Initialization Files -->
<script src="assets/js/theme.init.js"></script>