<footer id="footer" class="bg-color-quaternary position-relative mt-0">
    <svg class="custom-section-curved-top-7" width="100%" height="400" xmlns="http://www.w3.org/2000/svg">
        <path id="svg_2" fill="#171940" stroke="#000" stroke-width="0" d="m-16.68437,95.44889c0,0 5.33335,176.00075 660.00283,93.33373c327.33474,-41.33351 503.33549,15.3334 639.00274,35.66682c135.66725,20.33342 59.66691,9.66671 358.33487,28.33346c298.66795,18.66676 268.66829,-45.00088 382.66831,-112.00048c114.00002,-66.9996 718.31698,-59.48704 1221.66946,95.563c503.35248,155.05004 -221.83202,184.10564 -243.66935,197.60521c-21.83733,13.49958 -3008.67549,-19.83371 -3008.00467,-20.83335c-0.67082,0.99964 -30.00428,-232.33469 -10.00419,-317.66839z" class="svg-fill-color-quaternary" />
    </svg>
    <div class="container mt-0 mb-4">
        <div class="row py-5">
            <div class="col-lg-3 mb-5 mb-lg-0">
                <h4 class="font-weight-bold text-color-light text-5 ls-0 pb-1 mb-2">ABOUT US</h4>
                <p>Codenya Studio Technologies enable clients to out perform the competition and stay ahead of the innovation curve. Team Codenya, works closely with you to develop the right solution for your business. From the conceptualization ...</p>
                <a href="demo-seo-2.html">
                    <img src="img/logo.png" class="img-fluid" alt="Demo SEO 2" />
                </a>
            </div>
            <div class="col-lg-3 mb-4 mb-lg-0">
                <h4 class="font-weight-bold text-color-light text-5 ls-0 pb-1 mb-2">USEFUL LINKS</h4>
                <ul class="list list-unstyled">
                    <li class="mb-1">
                        <a href="./index.php">Home</a>
                    </li>
                    <li class="mb-1">
                        <a href="./services.php">Services</a>
                    </li>
                    <li class="mb-1">
                        <a href="about-us.php">About Us</a>
                    </li>
                    <li class="mb-1">
                        <a href="contact-us.php">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 mb-4 mb-lg-0">
                <h4 class="font-weight-bold text-color-light text-5 ls-0 pb-1 mb-2">OUR SERVICES</h4>
                <ul class="list list-unstyled">
                    <li class="mb-1">
                        <a href="android-app-development.php">Android App Development</a>
                    </li>
                    <li class="mb-1">
                        <a href="ios-app-development-services.php">iOS App Development
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="mobile-app-design-services.php">Mobile App Design
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="mobile-app-marketing-services.php">Mobile App Marketing
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="web-development-services.php">Web Development</a>
                    </li>
                    <li class="mb-1">
                        <a href="search-engine-optimization-services.php">Search Engine Optimization
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="cross-platform-mobile-development-services.php">Cross Platform Mobile Development
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h4 class="font-weight-bold text-color-light text-5 ls-0 pb-1 mb-2">CONTACT US</h4>
                <ul class="list list-unstyled">
                    <li class="mb-1">
                        Address:
                        <a href="https://goo.gl/maps/wQMe6jSqa7VBjnyb8">
                            Satellite Town, Sargodha, Punjab, Pakistan
                        </a>
                    </li>
                    <li class="mb-1">
                        Phone: <a href="tel:+923335003357">(+92) 333-5003357</a>
                    </li>
                    <li class="mb-1">
                        Email: <a href="mailto:codenya.studio@gmail.com">codenya.studio@gmail.com</a>
                    </li>
                </ul>
                <ul class="social-icons custom-social-icons-style-1">
                    <li class="social-icons-twitter"><a href="https://api.whatsapp.com/send?phone=923335003357" target=" _blank" title="Twitter"><i class="fab fa-whatsapp" style="font-weight: 900!important;"></i></a></li>
                    <li class="social-icons-instagram"><a href="mailto:codenya.studio@gmail.com" target="_blank" title="Gmail"><i class="fas fa-envelope"></i></a></li>
                    <li class="social-icons-facebook"><a href="https://www.facebook.com/CodenyaPK" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                </ul>

            </div>
        </div>
    </div>
    <div class="footer-copyright bg-color-quaternary">
        <div class="container py-2">
            <hr class="bg-color-light opacity-1 mb-0">
            <div class="row justify-content-center py-4">
                <div class="col-auto">
                    <p class="text-color-light opacity-5 text-3">Copyright © <script>
                            var d = new Date();
                            var n = d.getFullYear();
                            document.write(n);
                        </script>
                        Codenya Studio(Design & Software Solutions).</p>
                </div>
            </div>
        </div>
    </div>
</footer>