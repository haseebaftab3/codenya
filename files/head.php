<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Codenya Studio | Strategy | Technology</title>

<meta name="keywords" content="Strategy,Technology,Codenya Studio" />
<meta name="description" content="Codenya Studio Technologies enable clients to out perform the competition and stay ahead of the innovation curve. Team Codenya, works closely with you to develop the right solution for your business. From the conceptualization of the project to it's development, we work in partnership with you to deliver the best bespoke products/services that support your vision.">
<meta name="author" content="Codenya Studio">

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="assets/vendor/animate/animate.min.css">
<link rel="stylesheet" href="assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="assets/css/theme.css">
<link rel="stylesheet" href="assets/css/theme-elements.css">
<link rel="stylesheet" href="assets/css/theme-blog.css">
<link rel="stylesheet" href="assets/css/theme-shop.css">

<!-- Current Page CSS -->
<link rel="stylesheet" href="assets/vendor/rs-plugin/css/settings.css">
<link rel="stylesheet" href="assets/vendor/rs-plugin/css/layers.css">
<link rel="stylesheet" href="assets/vendor/rs-plugin/css/navigation.css">

<!-- Demo CSS -->
<link rel="stylesheet" href="assets/css/demos/demo-seo-2.css">

<!-- Skin CSS -->
<link rel="stylesheet" href="assets/css/skins/skin-seo-2.css">
<script src="assets/master/style-switcher/style.switcher.localstorage.js"></script>

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="assets/css/custom.css">

<!-- Head Libs -->
<script src="assets/vendor/modernizr/modernizr.min.js"></script>