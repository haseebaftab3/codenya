<!DOCTYPE html>
<html data-style-switcher-options="{'changeLogo': false, 'colorPrimary': '#e23c8e', 'colorSecondary': '#a33188', 'colorTertiary': '#4046b3', 'colorQuaternary': '#171940'}">



<head>
	<?php require("./files/head.php") ?>
</head>

<body data-target="#header" data-spy="scroll" data-offset="100">

	<div class="body">
		<?php require("./files/header.php") ?>


		<div role="main" class="main">

			<section class="page-header page-header-lg custom-bg-color-light-1 border-0 m-0">
				<div class="container position-relative pt-5 pb-4 mt-5">
					<div class="custom-circle custom-circle-wrapper custom-circle-big custom-circle-pos-1 custom-circle-pos-1-1 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="900" data-appear-animation-duration="2s">
						<div class="bg-color-tertiary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-2 custom-circle-pos-2-2 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1450" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.2, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-3 custom-circle-pos-3-3 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1300">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-small custom-circle-pos-4 custom-circle-pos-4-4 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1600">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.6, 'transition': true, 'transitionDuration': 500}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-5 custom-circle-pos-5-5 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1050" data-appear-animation-duration="2s">
						<div class="bg-color-secondary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.2, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-6 custom-circle-pos-6-6 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1200" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.8, 'transition': true, 'transitionDuration': 500}"></div>
					</div>
					<div class="row py-5 mb-5 mt-2 p-relative z-index-1">
						<div class="col">
							<div class="overflow-hidden">
								<ul class="breadcrumb d-block text-center appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="800">
									<li><a href="./index.php">Home</a></li>
									<li><a href="./services.php">Services</a></li>
									<li class="active">Web Development</li>
								</ul>
							</div>
							<div class="overflow-hidden mb-4">
								<h1 class="d-block text-color-quaternary font-weight-bold text-center line-height-4 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1000">Web Development</h1>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="section bg-color-light position-relative border-0 pt-0 m-0">
				<svg class="custom-page-header-curved-top-1" width="100%" height="700" xmlns="http://www.w3.org/2000/svg">
					<path transform="rotate(-3.1329219341278076 1459.172607421877,783.5322875976566) " d="m-12.54488,445.11701c0,0 2.16796,-1.48437 6.92379,-3.91356c4.75584,-2.42918 12.09956,-5.80319 22.45107,-9.58247c20.70303,-7.55856 53.43725,-16.7382 101.56202,-23.22255c48.12477,-6.48434 111.6401,-10.27339 193.90533,-7.05074c41.13262,1.61132 88.20271,5.91306 140.3802,12.50726c230.96006,32.89734 314.60609,102.57281 635.26547,59.88645c320.65938,-42.68635 452.47762,-118.72154 843.58759,3.72964c391.10997,122.45118 553.23416,-82.15958 698.49814,-47.66481c-76.25064,69.23438 407.49874,281.32592 331.2481,350.5603c-168.91731,29.52009 85.02254,247.61162 -83.89478,277.13171c84.07062,348.27313 -2948.95065,-242.40222 -2928.39024,-287.84045" stroke-width="0" stroke="#000" fill="#FFF" id="svg_2" />
				</svg>
				<div class="container pb-2 mb-4">
					<div class="row justify-content-center align-items-center">
						<div class="col-md-10 col-lg-6 mb-5 mb-lg-0">
							<h2 class="text-color-dark font-weight-semibold text-6 line-height-3 mb-0 pr-5 mr-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1600">Web Development</h2>
							<!-- <span class="d-block mb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1800">THE EASIEST WAY</span> -->
							<p class="lead pr-5 mb-4 pb-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="2000">After since the commercialization of the web, the web development industry is being on the continuous growth. And the ultimate reason for this growth in web industry is because of the businesses and enterprises which are always in seek of any opportunity to use their sites for selling their products and services to their clients. Through out of box thinking we believe in hand coded web development services with fully customization options for basic websites to highly complex and structured websites.</p>
							<div class="feature-box appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="2200">
								<div class="feature-box-icon custom-feature-box-icon-size-1 bg-color-secondary top-0">
									<i class="fas fa-puzzle-piece position-relative left-1"></i>
								</div>
								<div class="feature-box-info mb-4 pb-3">
									<h4 class="font-weight-bold line-height-3 custom-font-size-1 mb-1">Web Development Services</h4>
									<p class="mb-0">What does innovation look like? How can one generate more business right in this cutthroat business world? Let us save your cost and increase your productivity. We provide smart Web Development Services in close cooperation with our customers, transforming a customer idea into a working solution.</p>
								</div>
							</div>
							<div class="feature-box appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="2400">
								<div class="feature-box-icon custom-feature-box-icon-size-1 bg-color-tertiary top-0">
									<i class="fas fa-map-signs position-relative top-2 right-1"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="font-weight-bold line-height-3 custom-font-size-1 mb-1">Web development services</h4>
									<p class="mb-0">The foremost part of our web development services is the Design & Usability of the website because if your website fails to provide the desired information to your users with ease in navigation, then more chances are they will leave your site in time span of 3 to 5 seconds. And maybe they will never come to your site again! We just don’t develop customized web pages for our clients, but we also provide the best web solutions for our clients which could lead them to enhance their market share, growth, sales, & business, and can help them to achieve their goals & objectives in the set time span.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-5 offset-lg-1 pl-5 pb-lg-3 mb-md-4 mb-lg-5 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="2200">
							<img src="img/demos/seo-2/icons/target.svg" class="img-fluid mb-4 pb-2" width="100%" height="100%" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-tertiary'}" />
						</div>
						<div class="col-md-10 col-lg-12 mt-4">
							<p class="mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">Our team of passionate strategists, experienced designers, dynamic developers, and smart engineers will work with you side by side to create you the best possible solution out of it. If you’re looking to sell your products or services online, or you’re already using some e-commerce platform and looking to generate more business, we can definitely help you in getting there fast. We can develop and customize all kind of web development solutions from a small sized firm to a mid ranged and a large multi-site enterprise organization. We believe in helping businesses through technology.
								<br>
								<br>
								We can develop and customize all kind of web development solutions from a small sized firm to a mid ranged and a large multi-site enterprise organization. We believe in helping businesses through technology.
								<br>
								<br>
								Whether you have a limited budget or a substantial budget, we have enough opportunities for you to gain maximum return on investment to take your business or company to the next level. We feel pride on offering amazing, affordable and professional web development services for business professionals in just about every industry. Let us bring you a strong online presence
							</p>

							<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="350"> <a href="./contact-us.php">Contact us</a> to learn how we can bring you a strong online presence today by implementing the best Web Development Strategy for your organization.</p>
						</div>
					</div>
				</div>
			</section>



			<?php require("./files/newsletter.php") ?>


		</div>

		<?php require("./files/footer.php") ?>


	</div>

	<?php require("./files/js.php") ?>

</body>


</html>