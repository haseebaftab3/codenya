<!DOCTYPE html>
<html data-style-switcher-options="{'changeLogo': false, 'colorPrimary': '#e23c8e', 'colorSecondary': '#a33188', 'colorTertiary': '#4046b3', 'colorQuaternary': '#171940'}">



<head>
	<?php require("./files/head.php") ?>
</head>

<body data-target="#header" data-spy="scroll" data-offset="100">

	<div class="body">
		<?php require("./files/header.php") ?>


		<div role="main" class="main">

			<section class="page-header page-header-lg custom-bg-color-light-1 border-0 m-0">
				<div class="container position-relative pt-5 pb-4 mt-5">
					<div class="custom-circle custom-circle-wrapper custom-circle-big custom-circle-pos-1 custom-circle-pos-1-1 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="900" data-appear-animation-duration="2s">
						<div class="bg-color-tertiary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-2 custom-circle-pos-2-2 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1450" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.2, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-3 custom-circle-pos-3-3 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1300">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-small custom-circle-pos-4 custom-circle-pos-4-4 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1600">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.6, 'transition': true, 'transitionDuration': 500}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-5 custom-circle-pos-5-5 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1050" data-appear-animation-duration="2s">
						<div class="bg-color-secondary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.2, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-6 custom-circle-pos-6-6 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1200" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.8, 'transition': true, 'transitionDuration': 500}"></div>
					</div>
					<div class="row py-5 mb-5 mt-2 p-relative z-index-1">
						<div class="col">
							<div class="overflow-hidden">
								<ul class="breadcrumb d-block text-center appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="800">
									<li><a href="./index.php">Home</a></li>
									<li><a href="./services.php">Services</a></li>
									<li class="active">Search Engine Optimization</li>
								</ul>
							</div>
							<div class="overflow-hidden mb-4">
								<h1 class="d-block text-color-quaternary font-weight-bold text-center line-height-4 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1000">Search Engine Optimization</h1>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="section bg-color-light position-relative border-0 pt-0 m-0">
				<svg class="custom-page-header-curved-top-1" width="100%" height="700" xmlns="http://www.w3.org/2000/svg">
					<path transform="rotate(-3.1329219341278076 1459.172607421877,783.5322875976566) " d="m-12.54488,445.11701c0,0 2.16796,-1.48437 6.92379,-3.91356c4.75584,-2.42918 12.09956,-5.80319 22.45107,-9.58247c20.70303,-7.55856 53.43725,-16.7382 101.56202,-23.22255c48.12477,-6.48434 111.6401,-10.27339 193.90533,-7.05074c41.13262,1.61132 88.20271,5.91306 140.3802,12.50726c230.96006,32.89734 314.60609,102.57281 635.26547,59.88645c320.65938,-42.68635 452.47762,-118.72154 843.58759,3.72964c391.10997,122.45118 553.23416,-82.15958 698.49814,-47.66481c-76.25064,69.23438 407.49874,281.32592 331.2481,350.5603c-168.91731,29.52009 85.02254,247.61162 -83.89478,277.13171c84.07062,348.27313 -2948.95065,-242.40222 -2928.39024,-287.84045" stroke-width="0" stroke="#000" fill="#FFF" id="svg_2" />
				</svg>
				<div class="container pb-2 mb-4">
					<div class="row justify-content-center align-items-center">
						<div class="col-md-10 col-lg-6 mb-5 mb-lg-0">
							<h2 class="text-color-dark font-weight-semibold text-6 line-height-3 mb-0 pr-5 mr-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1600">Search Engine Optimization</h2>
							<!-- <span class="d-block mb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1800">THE EASIEST WAY</span> -->
							<p class="lead pr-5 mb-4 pb-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="2000">Search Engine Optimization commonly known as SEO is basically a strategic process that is used to improve a website or web page’s visibility along with ranking in search engines organic placement especially on Google.com.
								<br>
								Are you being found online? Are you trying to take your business website to the first page of search engine results? Is your traffic converting at lower rates than you expected? If so, it means your website probably lacks the proper SEO which required to maximize your business visibility on internet.</p>
							<div class="feature-box appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="2200">
								<div class="feature-box-icon custom-feature-box-icon-size-1 bg-color-secondary top-0">
									<i class="fas fa-puzzle-piece position-relative left-1"></i>
								</div>
								<div class="feature-box-info mb-4 pb-3">
									<h4 class="font-weight-bold line-height-3 custom-font-size-1 mb-1">Increase your business leads, calls and sales</h4>
									<p class="mb-0">
										If you can manage our Search Engine Optimization (SEO) Services intelligently and properly, it can save you hundreds of thousands of your dollars. Our prioritize goal is to increase your business leads, calls and sales through our Search Engine Optimization Services done right.
									</p>
								</div>
							</div>
							<div class="feature-box appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="2400">
								<div class="feature-box-icon custom-feature-box-icon-size-1 bg-color-tertiary top-0">
									<i class="fas fa-map-signs position-relative top-2 right-1"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="font-weight-bold line-height-3 custom-font-size-1 mb-1">Planned search engine optimization</h4>
									<p class="mb-0">Every website is unique when it comes to us. We circumspectly analyze & measure how well is your site optimized, we research about your competitors and ensure your traffic potential on site. With our planned search engine optimization strategies and practiced marketing tactics, we guarantee you in achieving long term success at Google SERPS on behalf of keywords placements, ultimately bringing traffic and customers to your site. With our experienced SEO services, your site will not only rank well in the search engine results but will also bring leads converting to sales.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-5 offset-lg-1 pl-5 pb-lg-3 mb-md-4 mb-lg-5 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="2200">
							<img src="img/demos/seo-2/icons/target.svg" class="img-fluid mb-4 pb-2" width="100%" height="100%" alt="" data-icon data-plugin-options="{'onlySVG': true, 'extraClass': 'svg-fill-color-tertiary'}" />
						</div>
						<div class="col-md-10 col-lg-12 mt-4">
							<p class="mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">
								With a team of highly talented SEO specialists and consultants, we have specially designed our Search Engine Optimization (SEO) Services to increase your site ranking in search results in order to deliver high traffic and quality leads to your site. All of our SEO work is totally handmade, there are no such artificial bots in our organic SEO services and every SEO campaign we work on is totally adaptive to your business, to the industry, and to your team.
								<br>
								<br>
								Our SEO team has a track record of spreading your online business voice across blogs, articles, web pages, social media channels, directories, business listings, and much you that you haven’t envisioned. We boost your SEO results with our proven Search Engine Optimization Services that outlast trends, by using the entire search engine page to build your business, while helping your customers to find exactly what they want.
								<br>
								<br>
								So if you want your site to be ranked in Free, Organic and Natural Search Results on Search Engines especially Google.com, make sure you’re using our Search Engine Optimization Services.
								<ul>
									<li><strong>SEO Consultancy</strong></li>
									<li><strong>Keywords &amp; Market Research</strong></li>
									<li><strong>Keywords Optimization</strong></li>
									<li><strong>On Page SEO</strong></li>
									<li><strong>Off Page SEO</strong></li>
									<li><strong>SEO Content Writing</strong></li>
									<li><strong>SEO Blogs</strong></li>
									<li><strong>SEO Articles</strong></li>
									<li><strong>SEO Alt Images</strong></li>
									<li><strong>SEO Technical Audits</strong></li>
									<li><strong>Link Building</strong></li>
									<li><strong>Website SEO Audit</strong></li>
									<li><strong>SEO Tags for Ranking Improvements</strong></li>
								</ul>
							</p>

						</div>
					</div>
				</div>
			</section>



			<?php require("./files/newsletter.php") ?>


		</div>

		<?php require("./files/footer.php") ?>


	</div>

	<?php require("./files/js.php") ?>

</body>


</html>