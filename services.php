<!DOCTYPE html>
<html data-style-switcher-options="{'changeLogo': false, 'colorPrimary': '#e23c8e', 'colorSecondary': '#a33188', 'colorTertiary': '#4046b3', 'colorQuaternary': '#171940'}">

<head>
	<?php require("./files/head.php") ?>
</head>


<body data-target="#header" data-spy="scroll" data-offset="100">

	<div class="body">
		<?php require("./files/header.php") ?>

		<div role="main" class="main">

			<section class="page-header page-header-lg custom-bg-color-light-1 border-0 m-0">
				<div class="container position-relative pt-5 pb-4 mt-5">
					<div class="custom-circle custom-circle-wrapper custom-circle-big custom-circle-pos-1 custom-circle-pos-1-1 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="900" data-appear-animation-duration="2s">
						<div class="bg-color-tertiary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-2 custom-circle-pos-2-2 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1450" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.2, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-3 custom-circle-pos-3-3 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1300">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.3, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-small custom-circle-pos-4 custom-circle-pos-4-4 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1600">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.6, 'transition': true, 'transitionDuration': 500}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-5 custom-circle-pos-5-5 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1050" data-appear-animation-duration="2s">
						<div class="bg-color-secondary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'top', 'speed': 0.2, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-6 custom-circle-pos-6-6 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1200" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.8, 'transition': true, 'transitionDuration': 500}"></div>
					</div>
					<div class="row py-5 mb-5 mt-2 p-relative z-index-1">
						<div class="col">
							<div class="overflow-hidden">
								<ul class="breadcrumb d-block text-center appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="800">
									<li><a href="demo-seo-2.html">Home</a></li>
									<li class="active">Services</li>
								</ul>
							</div>
							<div class="overflow-hidden mb-4">
								<h1 class="d-block text-color-quaternary font-weight-bold text-center mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1000">Services</h1>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="section bg-color-light position-relative border-0 pt-3 m-0">
				<svg class="custom-page-header-curved-top-1" width="100%" height="700" xmlns="http://www.w3.org/2000/svg">
					<path transform="rotate(-3.1329219341278076 1459.172607421877,783.5322875976566) " d="m-12.54488,445.11701c0,0 2.16796,-1.48437 6.92379,-3.91356c4.75584,-2.42918 12.09956,-5.80319 22.45107,-9.58247c20.70303,-7.55856 53.43725,-16.7382 101.56202,-23.22255c48.12477,-6.48434 111.6401,-10.27339 193.90533,-7.05074c41.13262,1.61132 88.20271,5.91306 140.3802,12.50726c230.96006,32.89734 314.60609,102.57281 635.26547,59.88645c320.65938,-42.68635 452.47762,-118.72154 843.58759,3.72964c391.10997,122.45118 553.23416,-82.15958 698.49814,-47.66481c-76.25064,69.23438 407.49874,281.32592 331.2481,350.5603c-168.91731,29.52009 85.02254,247.61162 -83.89478,277.13171c84.07062,348.27313 -2948.95065,-242.40222 -2928.39024,-287.84045" stroke-width="0" stroke="#000" fill="#FFF" id="svg_2" />
				</svg>
				<div class="container position-relative z-index-1 pb-3">
					<div class="custom-circle custom-circle-medium custom-circle-pos-17 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1200">
						<div class="bg-color-quaternary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-small custom-circle-pos-18 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1400">
						<div class="custom-bg-color-grey-1 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 2500}"></div>
					</div>
					<div class="custom-circle custom-circle-extra-small custom-circle-pos-19 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1600">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>

					<div class="row justify-content-center pb-2 mb-4">
						<div class="col-md-7 col-lg-4 mb-4 mb-lg-0">
							<div class="card border-0 custom-box-shadow-1 custom-border-radius-1 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" data-plugin-options="{'accY': -100}">
								<div class="custom-dots-rect-3" style="background-image: url(img/demos/seo-2/dots-group-3.png);"></div>
								<div class="card-body text-center p-5">
									<img src="img/demos/seo-2/icons/icon-3.png" class="img-fluid mb-4 mt-3 pb-3" width="55" alt="" />
									<h4 class="text-color-dark font-weight-semibold mb-3">Mobile App(Android and IOS ) Development</h4>
									<p>Our team of passionate strategists, experienced web designers, dynamic developers, and smart engineers has the skills to bring your ideas to life with the help of a Mobile App.</p>
									<a href="demo-seo-2-services-detail.html" class="text-color-tertiary font-weight-bold">READ MORE +</a>
								</div>
							</div>
							<div class="card border-0 custom-box-shadow-1 custom-border-radius-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400" data-plugin-options="{'accY': -100}">
								<div class="card-body text-center p-5">
									<img src="img/demos/seo-2/icons/icon-4.png" class="img-fluid mb-4 mt-3 pb-3" width="55" alt="" />
									<h4 class="text-color-dark font-weight-semibold mb-3">Website Design & Development</h4>
									<p>If you’re looking to sell your products or services online, or you’re already using some e-commerce platform and looking to generate more business.</p>
									<a href="demo-seo-2-services-detail.html" class="text-color-tertiary font-weight-bold">READ MORE +</a>
								</div>
							</div>
						</div>
						<div class="col-md-7 col-lg-4 pt-lg-4 mt-lg-5 mb-4 mb-lg-0">
							<div class="card border-0 custom-box-shadow-1 custom-border-radius-1 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">
								<div class="card-body text-center p-5">
									<img src="img/demos/seo-2/icons/icon-2.png" class="img-fluid mb-4 mt-3 pb-3" width="55" alt="" />
									<h4 class="text-color-dark font-weight-semibold mb-3">Branding & Online
										Marketing</h4>
									<p>We ensure, with our Social Media Services, your business can quickly access to millions of potential customers just through key social media sites.</p>
									<a href="demo-seo-2-services-detail.html" class="text-color-tertiary font-weight-bold">READ MORE +</a>
								</div>
							</div>
							<div class="card border-0 custom-box-shadow-1 custom-border-radius-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250">
								<div class="card-body text-center p-5">
									<img src="img/demos/seo-2/icons/icon-5.png" class="img-fluid mb-4 mt-3 pb-3" width="55" alt="" />
									<h4 class="text-color-dark font-weight-semibold mb-3">Social Media</h4>
									<p>We usually perceive social media marketing to promote website or mobile app on various social media Platforms.</p>
									<a href="demo-seo-2-services-detail.html" class="text-color-tertiary font-weight-bold">READ MORE +</a>
								</div>
							</div>
						</div>
						<div class="col-md-7 col-lg-4">
							<div class="card border-0 custom-box-shadow-1 custom-border-radius-1 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-plugin-options="{'accY': -100}">
								<div class="card-body text-center p-5">
									<img src="img/demos/seo-2/icons/icon-1.png" class="img-fluid mb-4 mt-3 pb-3" width="55" alt="" />
									<h4 class="text-color-dark font-weight-semibold mb-3">Branding & Online Marketing</h4>
									<p>We ensure, with our Social Media Services, your business can quickly access to millions of potential customers just through key social media sites.</p>
									<a href="demo-seo-2-services-detail.html" class="text-color-tertiary font-weight-bold">READ MORE +</a>
								</div>
							</div>
							<div class="card border-0 custom-box-shadow-1 custom-border-radius-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500" data-plugin-options="{'accY': -100}">
								<div class="card-body text-center p-5">
									<img src="img/demos/seo-2/icons/icon-6.png" class="img-fluid mb-4 mt-3 pb-3" width="55" alt="" />
									<h4 class="text-color-dark font-weight-semibold mb-3">Cross Platform Mobile Development</h4>
									<p>We have build the many innovative cross platform mobile apps for several categories and industries including e-commerce, travel, entertainment, photography, news, music & video, etc.</p>
									<a href="demo-seo-2-services-detail.html" class="text-color-tertiary font-weight-bold">READ MORE +</a>
								</div>
							</div>
						</div>
					</div>
					<div class="custom-circle custom-circle-big custom-circle-pos-20 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="200" data-appear-animation-duration="2s">
						<div class="bg-color-secondary rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 1, 'transition': true, 'transitionDuration': 2000}"></div>
					</div>
					<div class="custom-circle custom-circle-small custom-circle-pos-21 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="700" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 1000}"></div>
					</div>
					<div class="custom-circle custom-circle-medium custom-circle-pos-22 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1200" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-1 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 3000}"></div>
					</div>
					<div class="custom-circle custom-circle-extra-small custom-circle-pos-23 appear-animation" data-appear-animation="expandInWithBlur" data-appear-animation-delay="1700" data-appear-animation-duration="2s">
						<div class="custom-bg-color-grey-2 rounded-circle w-100 h-100" data-plugin-float-element data-plugin-options="{'startPos': 'bottom', 'speed': 0.5, 'transition': true, 'transitionDuration': 1500}"></div>
					</div>
				</div>
			</section>

			<?php require("./files/newsletter.php") ?>


		</div>

		<?php require("./files/footer.php") ?>

	</div>
	<?php require("./files/js.php") ?>
</body>


</html>